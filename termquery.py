#! /usr/bin/python3
# Control Sequence Introducer
CSI = "\033["

# Device Control String
DCS = "\033P"
STRING_TERMINATOR = "\033\\"

def termquery(query, terminator, output_prefix=CSI):
    import sys
    import tty
    import termios
    import select

    TIMEOUT = 0.1

    fd = sys.stdin.fileno()
    orig_attr = termios.tcgetattr(fd)
    try:
        #sys.stderr.write(f"Writing: {repr(CSI+query)}\n"); sys.stderr.flush()
        tty.setraw(fd)
        sys.stdout.write(CSI+query)
        sys.stdout.flush()
        readable = select.select([sys.stdout],[],[],TIMEOUT)[0]
        if len(readable) == 0:
            #print("No reply")
            return None
        maybe_csi = sys.stdin.read(len(output_prefix))
        assert maybe_csi == output_prefix, f'expected CSI "{repr(output_prefix)}", got "{repr(maybe_csi)}"'
        out = [""]
        while True:
            #sys.stderr.write("reading\n"); sys.stderr.flush()
            c = sys.stdin.read(1)
            #print(">",c,"<")
            if c == ";":
                out += [""]
            else:
                out[-1] += c
                if out[-1].endswith(terminator):
                    out[-1] = out[-1][:-len(terminator)]
                    return out
                if c == terminator:
                    return out #list([int(x) for x in out])
    finally:
        termios.tcsetattr(fd, termios.TCSADRAIN, orig_attr)



