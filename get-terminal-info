#! /usr/bin/python3
import array, fcntl, sys, termios
buf = array.array('H', [0, 0, 0, 0])
fcntl.ioctl(sys.stdout, termios.TIOCGWINSZ, buf)
ht_chars, wd_chars, wd_px, ht_px = buf
print(f"width:  {wd_chars:4d} characters, {wd_px:5d} pixels")
print(f"height: {ht_chars:4d} characters, {ht_px:5d} pixels")

from termquery import STRING_TERMINATOR, DCS, termquery
csi_query = termquery
"""
def csi_query_nonblock(query, terminator):
    import sys
    import tty
    import termios
    import select
    import os
    import fcntl

    TIMEOUT = 0.1

    orig_fl = fcntl.fcntl(sys.stdin, fcntl.F_GETFL)

    try:
        sys.stderr.write(f"Writing: {repr(CSI+query)}\n"); sys.stderr.flush()
        fcntl.fcntl(sys.stdin, fcntl.F_SETFL, orig_fl | os.O_NONBLOCK)
        sys.stdout.write(CSI+query)
        sys.stdout.flush()
        select.select([sys.stdin],[],[],1)
        maybe_csi = sys.stdin.read(len(CSI))
        assert maybe_csi == CSI, f'expected CSI {repr(CSI)}, got {repr(maybe_csi)}'
        out = [""]
        while True:
            #sys.stderr.write("reading\n"); sys.stderr.flush()
            c = sys.stdin.read(1)
            #print(">",c,"<")
            if c == terminator:
                return list([int(x) for x in out])
            if c == ";":
                out += [""]
            else:
                out[-1] += c
    finally:
        fcntl.fcntl(sys.stdin, fcntl.F_SETFL, orig_fl)
csi_query = csi_query_nonblock
"""

def is_terminal_okay():
    ret = csi_query("5n", "n")
    if ret is None: return None
    return int(ret[0])

def get_cursor_position():
    ret = csi_query("6n", "R")
    if ret is None: return None
    return [int(ret[0]), int(ret[1])]

def get_terminal_size_characters():
    ret = csi_query("18t", "t")
    if ret is None: return None
    return [int(ret[1]), int(ret[2])]

def get_terminal_size_pixels():
    ret = csi_query("14t", "t")
    if ret is None: return None
    return [int(ret[1]), int(ret[2])]

def get_screen_size_pixels():
    ret = csi_query("15t", "t")
    if ret is None: return None
    return [int(ret[1]), int(ret[2])]

def get_character_size_pixels():
    ret = csi_query("16t", "t")
    if ret is None: return None
    return [int(ret[1]), int(ret[2])]

def get_number_color_registers():
    ret = csi_query("?1;1;0S", "S")
    if ret is None: return None
    if ret[1] != "0": return None
    return int(ret[2])

def get_number_maximum_color_registers():
    ret = csi_query("?1;4;0S", "S")
    if ret is None: return None
    if ret[1] != "0": return None
    return int(ret[2])

def get_geometry(num, max):
    maxn = 1
    if max: maxn = 4
    ret = csi_query(f"?{num};{maxn};0S", "S")
    if ret is None: return None
    if ret[1] != "0": return None
    return [int(ret[2]),int(ret[3])]

def get_sixel_geometry():         return get_geometry(2,max=False)
def get_maximum_sixel_geometry(): return get_geometry(2,max=True)
def get_regis_geometry():         return get_geometry(3,max=False)
def get_maximum_regis_geometry(): return get_geometry(3,max=True)

# AKA tertiary device attributes
def get_terminal_unit_id():
    ret = csi_query("=0c", STRING_TERMINATOR, output_prefix=DCS)
    return ret

def get_secondary_device_attributes():
    terminals = {
        ">0": "VT100",
        ">1": "VT220",
        ">2": "VT240 or VT241",
        ">18": "VT330",
        ">19": "VT340",
        ">24": "VT320",
        ">32": "VT382",
        ">41": "VT420",
        ">61": "VT510",
        ">64": "VT520",
        ">65": "VT525",
        }
    ret = csi_query(">c", "c")
    if ret is None: return None
    term = "Unknown (missing)"
    if len(ret) > 0:
        term = terminals.get(ret[0], f"Unknown {ret[0]}")
    firmware = "Unknown (missing)"
    if len(ret) > 1:
        firmware = ret[1]
    rom_cartridge_registration_number = "Unknown (missing)"
    if len(ret) > 2:
        rom_cartridge_registration_number = ret[2]
    return term, firmware, rom_cartridge_registration_number



def get_device_attributes():
    # Based on https://invisible-island.net/xterm/ctlseqs/ctlseqs.html#h3-Sixel-Graphics
    terminals = {
        #?1 ("VT100 with Advanced Video Option") # need ret[1] to distinguish
        #?1: "VT101 with No Options") # need ret[1] to distinguish
        "?4": "VT132 with Advanced Video and Graphics",
        "?6": "VT102",
        "?7": "VT131",
        "?12": "VT125",
        "?62": "VT220",
        "?63": "VT320",
        "?64": "VT420",
        }
    features = {
        "1":  "132-columns",
        "2":  "Printer",
        "3":  "ReGIS graphics",
        "4":  "Sixel graphics",
        "6":  "Selective erase",
        "8":  "User-defined keys",
        "9":  "National Replacement Character sets",
        "15": "Technical characters",
        "16": "Locator port",
        "17": "Terminal state interrogation",
        "18": "User windows",
        "21": "Horizontal scrolling",
        "22": "ANSI color, e.g., VT525",
        "28": "Rectangular editing",
        "29": "ANSI text locator (i.e., DEC Locator mode)",
        }

    ret = csi_query("c", "c")
    if ret is None:
        return "Unknown", []

    if ret[0] == "?1":
        if len(ret) > 0 and ret[1] == "2": term = "VT100 with Advanced Video Option"
        elif len(ret) > 0 and ret[1] == "0": term = "VT101 with No Options"
        else: "possibly VT100 or VT101 with unknown options"
    else:
        term = terminals.get(ret[0], f"unknown {ret[0]}")

    flags = []
    for arg in ret[1:]:
        desc = features.get(arg, f"unknown {arg}")
        flags.append(desc)

    return term, flags

def get_terminal_name_and_version():
    ret = csi_query(">0q", STRING_TERMINATOR, output_prefix=DCS+">|")
    return ret[0]

UNKNOWN="unknown (failed to detect)"

ret = is_terminal_okay()
out = "Terminal status:"
if ret is None: print(out,UNKNOWN)
elif ret == 0: print(out,"okay (it will probably always report this)")
elif ret == 1: print(out,"there is a problem")
else: print(out,"error detecting",ret)

terminal1, flags = get_device_attributes()
terminal2, firmware, cart = get_secondary_device_attributes()
terminal3 = get_terminal_name_and_version()
print("Terminal:", terminal1, "(as reported by primary query)")
print("Terminal:", terminal2, "(as reported by secondary query)")
print("Terminal:", terminal3, "(as reported by report name and version)")
print("Features:", ", ".join(flags))

print("Firmware version:", firmware)
print("ROM cartridge registration number:", cart)

print("Terminal Unit ID:", get_terminal_unit_id())

ret = get_cursor_position()
out = "Cursor position:"
if ret is None: print(out,UNKNOWN)
else: print(out, f"line {ret[0]}, column {ret[1]}")

ret = get_terminal_size_characters()
out = "Terminal size (characters):"
if ret is None: print(out,UNKNOWN)
else: print(out, f"{ret[0]} tall, {ret[1]} wide")

ret = get_terminal_size_pixels()
out = "Terminal size (pixels):"
if ret is None: print(out,UNKNOWN)
else: print(out, f"{ret[0]} tall, {ret[1]} wide")

ret = get_screen_size_pixels()
out = "Screen size (pixels):"
if ret is None: print(out,UNKNOWN)
else: print(out, f"{ret[0]} tall, {ret[1]} wide")

ret = get_character_size_pixels()
out = "Character size (pixels):"
if ret is None: print(out,UNKNOWN)
else: print(out, f"{ret[0]} tall, {ret[1]} wide")

ret = get_number_color_registers()
out = "Number of color registers:"
if ret is None: print(out,UNKNOWN)
else: print(out, ret)

ret = get_number_maximum_color_registers()
out = "Maximum number of color registers:"
if ret is None: print(out,UNKNOWN)
else: print(out, ret)

ret = get_sixel_geometry()
out = "Sixel geometry:"
if ret is None: print(out,UNKNOWN)
else: print(out, ret)

ret = get_maximum_sixel_geometry()
out = "Maximum sixel geometry:"
if ret is None: print(out,UNKNOWN)
else: print(out, ret)

ret = get_regis_geometry()
out = "Regis geometry:"
if ret is None: print(out,UNKNOWN)
else: print(out, ret)

ret = get_maximum_regis_geometry()
out = "Maximum regis geometry:"
if ret is None: print(out,UNKNOWN)
else: print(out, ret)



# 6n should be cursor position line;column +"R"
# 5n  should always return "\033[0n (terminal functioning correctly) +"n"
# 14t should report height and width in pixels (text area) +"t"
# 16t character size in pixels 6;height;width +"t"
# 18t text area in characters 8;height;width +"t"




